//
//  PostDetailViewModel.m
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import "PostDetailViewModel.h"

@implementation PostDetailViewModel

    - (void)downloadPostCommentList:(int)postId {
        
        NSString* urlString = [NSString stringWithFormat: @"https://jsonplaceholder.typicode.com/comments?postId=%i", postId];
        
        [[[NetworkApi alloc] init] get:urlString :^(NSArray *responseArray) {
            
            NSMutableArray* tempPostCommentArray = [[NSMutableArray alloc] init];
            
            for (NSDictionary *postCommentDictionary in responseArray) {
                PostComment* postComment = [[PostComment alloc] initWithDictionary:postCommentDictionary];
                if (postComment) {
                    [tempPostCommentArray addObject:postComment];
                }
            }
            
            self.postCommentArray = tempPostCommentArray;
            
        }];
    }
        
@end

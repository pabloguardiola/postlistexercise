//
//  PostListViewModel.m
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import "PostListViewModel.h"

@implementation PostListViewModel
    
    - (void)downloadPostList {
    
        NSString* urlString = @"https://jsonplaceholder.typicode.com/posts";
        
        [[[NetworkApi alloc] init] get:urlString :^(NSArray *responseArray) {
            
            NSMutableArray *tempPostArray = [[NSMutableArray alloc] init];
            
            for (NSDictionary *postDictionary in responseArray) {
                Post* post = [[Post alloc] initWithDictionary:postDictionary];
                if (post) {
                    [tempPostArray addObject:post];
                }
            }

            self.postArray = tempPostArray;
    }];
    
}
    
@end

//
//  ImageListViewModel.m
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import "ImageListViewModel.h"

@implementation ImageListViewModel

    - (void)downloadImageList {
        
        NSString* urlString = @"https://jsonplaceholder.typicode.com/photos";
        
        [[[NetworkApi alloc] init] get:urlString :^(NSArray *responseArray) {
            
            NSMutableArray *tempImageItemArray = [[NSMutableArray alloc] init];

            for (NSDictionary *imageItemDictionary in responseArray) {
                ImageItem* imageItem = [[ImageItem alloc] initWithDictionary:imageItemDictionary];
                if (imageItem) {
                    [tempImageItemArray addObject:imageItem];
                }
            }
            
            self.imageItemArray = tempImageItemArray;
            
        }];
    }
    
@end

//
//  PostListViewModel.h
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkApi.h"
#import "Post.h"

@interface PostListViewModel : NSObject
    
    @property (strong, nonatomic) NSMutableArray* postArray;

    - (void)downloadPostList;
    
@end

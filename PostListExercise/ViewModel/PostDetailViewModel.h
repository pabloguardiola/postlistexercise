//
//  PostDetailViewModel.h
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkApi.h"
#import "PostComment.h"

@interface PostDetailViewModel : NSObject

    @property (strong, nonatomic) NSMutableArray* postCommentArray;
    
    - (void)downloadPostCommentList:(int)postId;
    
@end

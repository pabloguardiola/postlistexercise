//
//  ImageItem.h
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageItem : NSObject

    @property (nonatomic) int photoId;
    @property (nonatomic) int albumId;
    @property (strong, nonatomic) NSString* title;
    @property (strong, nonatomic) NSString* url;
    @property (strong, nonatomic) NSString* thumbnailUrl;
    
    -(instancetype)initWithDictionary:(NSDictionary*)dictionary;
    
@end

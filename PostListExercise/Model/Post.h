//
//  Post.h
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Post : NSObject
    
    @property (nonatomic) int postId;
    @property (nonatomic) int userId;
    @property (strong, nonatomic) NSString *title;
    @property (strong, nonatomic) NSString *body;

    -(instancetype)initWithDictionary:(NSDictionary*)dictionary;
    
@end

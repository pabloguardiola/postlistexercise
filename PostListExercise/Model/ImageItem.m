//
//  ImageItem.m
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import "ImageItem.h"
#define guard(CONDITION) if (CONDITION) {}

@implementation ImageItem

    -(instancetype)initWithDictionary:(NSDictionary *)dictionary {
        
        if (self = [self init]) {
            
            guard(
                  dictionary[@"id"]
                  && dictionary[@"albumId"]
                  && dictionary[@"title"]
                  && dictionary[@"url"]
                  && dictionary[@"thumbnailUrl"]
            ) else {
                return nil;
            }
            
            self.photoId = (int)dictionary[@"id"];
            self.albumId = (int)dictionary[@"albumId"];
            self.title = [NSString stringWithFormat:@"%@%@", [[(NSString*)dictionary[@"title"] substringToIndex:1] uppercaseString], [(NSString*)dictionary[@"title"] substringFromIndex:1]];
            self.url = (NSString*)dictionary[@"url"];
            self.thumbnailUrl = (NSString*)dictionary[@"thumbnailUrl"];
            
        }
        
        return self;
        
    }
    
@end

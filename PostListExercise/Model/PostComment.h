//
//  PostComment.h
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostComment : NSObject

    @property (nonatomic) int commentId;
    @property (nonatomic) int postId;
    @property (strong, nonatomic) NSString* name;
    @property (strong, nonatomic) NSString* email;
    @property (strong, nonatomic) NSString* body;
    
    -(instancetype)initWithDictionary:(NSDictionary*)dictionary;
    
@end

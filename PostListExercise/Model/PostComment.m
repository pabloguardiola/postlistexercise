//
//  PostComment.m
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import "PostComment.h"
#define guard(CONDITION) if (CONDITION) {}

@implementation PostComment

    -(instancetype)initWithDictionary:(NSDictionary *)dictionary {
        
        if (self = [self init]) {
            
            guard(
                  dictionary[@"id"]
                  && dictionary[@"postId"]
                  && dictionary[@"name"]
                  && dictionary[@"email"]
                  && dictionary[@"body"]
            ) else {
                return nil;
            }
            
            self.commentId = (int)dictionary[@"id"];
            self.postId = (int)dictionary[@"postId"];
            self.name = [NSString stringWithFormat:@"%@%@", [[(NSString*)dictionary[@"name"] substringToIndex:1] uppercaseString], [(NSString*)dictionary[@"name"] substringFromIndex:1]];
            self.email = (NSString*)dictionary[@"email"];
            self.body = [NSString stringWithFormat:@"%@%@", [[(NSString*)dictionary[@"body"] substringToIndex:1] uppercaseString], [(NSString*)dictionary[@"body"] substringFromIndex:1]];
            
        }
        
        return self;
        
    }
    
@end

//
//  Post.m
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import "Post.h"
#define guard(CONDITION) if (CONDITION) {}

@implementation Post

    -(instancetype)initWithDictionary:(NSDictionary *)dictionary {
        
        if (self = [self init]) {
            
            guard(
                  dictionary[@"id"]
                  && dictionary[@"userId"]
                  && dictionary[@"title"]
                  && dictionary[@"body"]
            ) else {
                return nil;
            }
            
            self.postId = (int)dictionary[@"id"];
            self.userId = (int)dictionary[@"userId"];
            self.title = [NSString stringWithFormat:@"%@%@", [[(NSString*)dictionary[@"title"] substringToIndex:1] uppercaseString], [(NSString*)dictionary[@"title"] substringFromIndex:1]];
            self.body =
            [NSString stringWithFormat:@"%@%@", [[(NSString*)dictionary[@"body"] substringToIndex:1] uppercaseString], [(NSString*)dictionary[@"body"] substringFromIndex:1]];
            
        }
        
        return self;
        
    }
    
@end

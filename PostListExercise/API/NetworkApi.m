//
//  NetworkApi.m
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import "NetworkApi.h"

@implementation NetworkApi
    
-(void)get:(NSString*)urlString :(GetCompletion)completion {
    
    if (self.getRequestTask != nil) {
        [self.getRequestTask cancel];
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLSession *session = [NSURLSession sharedSession];
    self.getRequestTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            completion(nil);
        }
        else {
            NSArray * parsedData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            completion(parsedData);
        }
    }];
    
    [self.getRequestTask resume];
}
    
-(RACSignal*)downloadImage:(NSString *)urlString {
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *downloadImageTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (error) {
                [subscriber sendError:error];
            }
            else {
                UIImage* image = [UIImage imageWithData:data];
                [subscriber sendNext:image];
                [subscriber sendCompleted];
            }
        }];
        
        [downloadImageTask resume];
        
        return [RACDisposable disposableWithBlock:^{
            [downloadImageTask cancel];
        }];
    }];
    
}

@end

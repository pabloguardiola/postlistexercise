//
//  NetworkApi.h
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>

@interface NetworkApi : NSObject
    
typedef void(^GetCompletion)(NSArray*);

@property (nonatomic, strong) NSURLSessionDataTask* getRequestTask;
    
- (void)get:(NSString*)urlString :(GetCompletion)completion;
    
- (RACSignal*)downloadImage:(NSString*)urlString;

@end

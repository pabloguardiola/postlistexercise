//
//  PostListTableView.m
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import "PostListTableView.h"

@interface PostListTableView ()

@end

@implementation PostListTableView

-(instancetype)initWithViewModel:(PostListViewModel *)postListViewModel {
    
    if (self = [super init]) {
        self.postListViewModel = postListViewModel;
    }
    
    return self;
}
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass: PostCell.self forCellReuseIdentifier: @"PostCell"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;
    
    self.navigationItem.title = @"Posts";
    
    [self.postListViewModel downloadPostList];
    
    [RACObserve(self.postListViewModel, postArray) subscribeNext:^(id _) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"List" style:UIBarButtonItemStylePlain target: self action: @selector(openImageList)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
    
- (void)openImageList {
    
    ImageListTableView* imageListViewController = [[ImageListTableView alloc] initWithViewModel: [[ImageListViewModel alloc] init]];
    
    [self.navigationController pushViewController:imageListViewController animated:true];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.postListViewModel.postArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PostCell *cell = (PostCell *)[tableView dequeueReusableCellWithIdentifier:@"PostCell"];
    
    Post* post = self.postListViewModel.postArray[indexPath.row];
    
    [cell.iconLabel setText: [NSString stringWithFormat:@"%i", post.userId]];
    [cell.titleLabel setText: post.title];
    [cell.bodyLabel setText: post.body];
    
    return cell;
}
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Post* post = self.postListViewModel.postArray[indexPath.row];
    
    PostDetailTableView* postDetailViewController = [[PostDetailTableView alloc] initWithPost: post :[[PostDetailViewModel alloc] init]];
    
    [self.navigationController pushViewController:postDetailViewController animated:true];
}

@end

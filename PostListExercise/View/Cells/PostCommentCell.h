//
//  PostCommentCell.h
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Masonry.h"

@interface PostCommentCell : UITableViewCell

    @property (strong, nonatomic) UIView* bubbleView;
    @property (strong, nonatomic) UIImageView* iconImageView;
    @property (strong, nonatomic) UILabel* titleLabel;
    @property (strong, nonatomic) UILabel* bodyLabel;
    
@end

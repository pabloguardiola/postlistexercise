//
//  ImageItemCell.h
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Masonry.h"

@interface ImageItemCell : UITableViewCell

    @property (strong, nonatomic) UIImageView* itemImageView;
    @property (strong, nonatomic) UILabel* titleLabel;
    
@end

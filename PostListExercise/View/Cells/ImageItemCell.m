//
//  ImageItemCell.m
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import "ImageItemCell.h"

@implementation ImageItemCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self initializeCell];
        
    }
    return self;
}
    
- (void)initializeCell {
    
    self.itemImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    
    [self.contentView addSubview: self.itemImageView];
    [self.contentView addSubview: self.titleLabel];
    
    self.itemImageView.clipsToBounds = YES;
    self.itemImageView.layer.cornerRadius = 8;
    
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    
    // AutoLayout
    UIEdgeInsets padding = UIEdgeInsetsMake(8, 8, 8, 8);
    CGFloat elementsOffset = 8;
    CGFloat iconViewSize = 50.0;
    
    [self.itemImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(iconViewSize));
        make.width.equalTo(@(iconViewSize));
        make.top.equalTo(self.contentView.mas_top).with.offset(padding.top);
        make.left.equalTo(self.contentView.mas_left).with.offset(padding.left);
        make.bottom.lessThanOrEqualTo(self.contentView.mas_bottom).with.offset(-padding.bottom);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).with.offset(padding.top);
        make.left.equalTo(self.itemImageView.mas_right).with.offset(elementsOffset);
        make.right.equalTo(self.contentView.mas_right).with.offset(-padding.right);
        make.bottom.lessThanOrEqualTo(self.contentView.mas_bottom).with.offset(-padding.bottom);
    }];
    
    [self.titleLabel sizeToFit];
    
}
    
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}
    
- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
}
    
- (void)prepareForReuse {
    self.itemImageView.image = nil;
}

@end

//
//  PostCommentCell.m
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import "PostCommentCell.h"

@implementation PostCommentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self initializeCell];
        
    }
    return self;
}
    
- (void)initializeCell {
    
    self.bubbleView = [[UIView alloc] initWithFrame:CGRectZero];
    self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.bodyLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    
    [self.contentView addSubview: self.bubbleView];
    [self.contentView addSubview: self.iconImageView];
    [self.bubbleView addSubview: self.titleLabel];
    [self.bubbleView addSubview: self.bodyLabel];
    
    self.bubbleView.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.1];
    self.bubbleView.clipsToBounds = YES;
    self.bubbleView.layer.cornerRadius = 12;
    
    self.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.iconImageView.clipsToBounds = YES;
    self.iconImageView.layer.borderWidth = 1;
    self.iconImageView.layer.borderColor = [[[UIColor blackColor] colorWithAlphaComponent:0.3] CGColor];
    self.iconImageView.image = [UIImage imageNamed:@"Profile"];
    
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    
    self.bodyLabel.numberOfLines = 0;
    self.bodyLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    
    // AutoLayout
    UIEdgeInsets padding = UIEdgeInsetsMake(8, 8, 8, 8);
    CGFloat elementsOffset = 8;
    CGFloat iconViewSize = 40.0;
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(iconViewSize));
        make.width.equalTo(@(iconViewSize));
        make.top.equalTo(self.contentView.mas_top).with.offset(padding.top);
        make.left.equalTo(self.contentView.mas_left).with.offset(padding.left);
    }];
    
    [self.bubbleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).with.offset(padding.top);
        make.left.equalTo(self.iconImageView.mas_right).with.offset(elementsOffset);
        make.right.equalTo(self.contentView.mas_right).with.offset(-padding.right);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-padding.bottom);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bubbleView.mas_top).with.offset(padding.top);
        make.left.equalTo(self.bubbleView.mas_left).with.offset(elementsOffset);
        make.right.equalTo(self.bubbleView.mas_right).with.offset(-padding.right);
    }];
    
    [self.bodyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).with.offset(elementsOffset);
        make.left.equalTo(self.titleLabel.mas_left);
        make.right.equalTo(self.bubbleView.mas_right).with.offset(-padding.right);
        make.bottom.equalTo(self.bubbleView.mas_bottom).with.offset(-padding.bottom);
    }];
    
    [self.titleLabel sizeToFit];
    [self.bodyLabel sizeToFit];
    
}
    
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    
}
    
- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    self.iconImageView.layer.cornerRadius = self.iconImageView.layer.frame.size.height / 2.0;
}

@end

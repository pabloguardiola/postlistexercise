//
//  PostTableViewCell.m
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import "PostCell.h"

@implementation PostCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self initializeCell];
        
    }
    return self;
}
    
- (void)initializeCell {
    
    self.iconImageView = [[UIView alloc] initWithFrame:CGRectZero];
    self.iconLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.bodyLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    
    [self.contentView addSubview: self.iconImageView];
    [self.contentView addSubview: self.iconLabel];
    [self.contentView addSubview: self.titleLabel];
    [self.contentView addSubview: self.bodyLabel];
    
    self.iconImageView.backgroundColor = [UIColor blackColor];
    self.iconImageView.clipsToBounds = YES;
    self.iconImageView.layer.cornerRadius = 8;
    
    self.iconLabel.textAlignment = NSTextAlignmentCenter;
    self.iconLabel.textColor = [UIColor whiteColor];
    
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    
    self.bodyLabel.numberOfLines = 0;
    self.bodyLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
    
    // AutoLayout
    UIEdgeInsets padding = UIEdgeInsetsMake(8, 8, 8, 8);
    CGFloat elementsOffset = 8;
    CGFloat iconViewSize = 50.0;
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(iconViewSize));
        make.width.equalTo(@(iconViewSize));
        make.top.equalTo(self.contentView.mas_top).with.offset(padding.top);
        make.left.equalTo(self.contentView.mas_left).with.offset(padding.left);
    }];
    
    [self.iconLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.iconImageView);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).with.offset(padding.top);
        make.left.equalTo(self.iconImageView.mas_right).with.offset(elementsOffset);
        make.right.equalTo(self.contentView.mas_right).with.offset(-padding.right);
    }];
    
    [self.bodyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).with.offset(elementsOffset);
        make.left.equalTo(self.titleLabel.mas_left);
        make.right.equalTo(self.contentView.mas_right).with.offset(-padding.right);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-padding.bottom);
    }];
    
    [self.titleLabel sizeToFit];
    [self.bodyLabel sizeToFit];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}
    
- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
}

@end

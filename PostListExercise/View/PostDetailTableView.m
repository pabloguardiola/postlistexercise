//
//  PostDetailTableView.m
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import "PostDetailTableView.h"

@interface PostDetailTableView ()

@end

@implementation PostDetailTableView

- (instancetype)initWithPost:(Post *)post :(PostDetailViewModel*)postDetailViewModel {
    
    if (self = [self init]) {
        self.post = post;
        self.postDetailViewModel = postDetailViewModel;
    }
    
    return self;
}
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass: PostCell.self forCellReuseIdentifier: @"PostCell"];
    [self.tableView registerClass: PostCommentCell.self forCellReuseIdentifier: @"PostCommentCell"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.allowsSelection = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (self.post) {
        self.navigationItem.title = [NSString stringWithFormat:@"Post %i", self.post.postId];
        [self.postDetailViewModel downloadPostCommentList: self.post.postId];
    }
    
    [RACObserve(self.postDetailViewModel, postCommentArray) subscribeNext:^(id _) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.postDetailViewModel.postCommentArray.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        PostCell *cell = (PostCell*)[tableView dequeueReusableCellWithIdentifier:@"PostCell" forIndexPath:indexPath];
        
        [cell.iconLabel setText: [NSString stringWithFormat:@"%i", self.post.userId]];
        [cell.titleLabel setText: self.post.title];
        [cell.bodyLabel setText: self.post.body];
        
        return cell;
    }
    else {
        int adjustedIndexRow = (int)indexPath.row - 1;
        
        PostCommentCell *cell = (PostCommentCell*)[tableView dequeueReusableCellWithIdentifier:@"PostCommentCell" forIndexPath:indexPath];
        
        PostComment* postComment = self.postDetailViewModel.postCommentArray[adjustedIndexRow];
        
        [cell.titleLabel setText: postComment.email];
        [cell.bodyLabel setText: postComment.body];
        
        return cell;
    }
    
}

@end

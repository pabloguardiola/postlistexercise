//
//  ImageListTableView.h
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "ImageListViewModel.h"
#import "ImageItemCell.h"

@interface ImageListTableView : UITableViewController

    @property (strong, nonatomic) ImageListViewModel* imageListViewModel;
    
    -(instancetype)initWithViewModel:(ImageListViewModel*)imageListViewModel;
    
@end

//
//  ImageListTableView.m
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import "ImageListTableView.h"

@interface ImageListTableView ()

@end

@implementation ImageListTableView

-(instancetype)initWithViewModel:(ImageListViewModel *)imageListViewModel {
    
    if (self = [self init]) {
        self.imageListViewModel = imageListViewModel;
    }
    
    return self;
}
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass: ImageItemCell.self forCellReuseIdentifier: @"ImageItemCell"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.allowsSelection = NO;
    
    [self.imageListViewModel downloadImageList];
    
    [RACObserve(self.imageListViewModel, imageItemArray) subscribeNext:^(id _) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.imageListViewModel.imageItemArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ImageItemCell *cell = (ImageItemCell*)[tableView dequeueReusableCellWithIdentifier:@"ImageItemCell" forIndexPath:indexPath];
    
    ImageItem* imageItem = self.imageListViewModel.imageItemArray[indexPath.row];
    
    [[[[NetworkApi alloc] init] downloadImage: imageItem.thumbnailUrl] subscribeNext:^(id  _Nullable x) {
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.itemImageView.image = (UIImage*)x;
        });
    }];
    [cell.titleLabel setText: imageItem.title];
    
    return cell;
}

@end

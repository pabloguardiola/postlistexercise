//
//  PostDetailTableView.h
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "Post.h"
#import "PostDetailViewModel.h"
#import "PostCell.h"
#import "PostCommentCell.h"
#import "PostComment.h"

@interface PostDetailTableView : UITableViewController

    @property (strong, nonatomic) PostDetailViewModel* postDetailViewModel;
    
    @property (strong, nonatomic) Post* post;
    
    - (instancetype)initWithPost:(Post*)post :(PostDetailViewModel*)postCommentViewModel;
    
@end

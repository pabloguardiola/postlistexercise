//
//  PostListTableView.h
//  PostListExercise
//
//  Created by Pablo Guardiola on 24/04/2018.
//  Copyright © 2018 Pablo Guardiola. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "PostListViewModel.h"
#import "ImageListViewModel.h"
#import "Post.h"
#import "PostCell.h"
#import "PostDetailTableView.h"
#import "ImageListTableView.h"

@interface PostListTableView : UITableViewController

    @property (strong, nonatomic) PostListViewModel* postListViewModel;
    
    -(instancetype)initWithViewModel:(PostListViewModel*)postListViewModel;
    
@end
